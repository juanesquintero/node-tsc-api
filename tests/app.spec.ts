/* eslint-disable @typescript-eslint/dot-notation */
import { App } from '../src/app'

describe('App', () => {
  let app = new App()
  const port = app['app'].get('port')

  describe('constructor()', () => {
    test('should create', () => {
      expect(app).toBeTruthy()
    })

    test('should call settings()', () => {
      const spySettings = jest.spyOn(App.prototype as any, 'settings')
      app = new App()
      expect(spySettings).toHaveBeenCalled()
    })
  })

  describe('settings()', () => {
    test('should set the port on express app object ()', () => {
      const spyAppSet = jest.spyOn(app['app'], 'set')
      app['settings']()
      expect(spyAppSet).toHaveBeenCalledWith('port', 3000)
    })

    test('should set by default 3000 port', () => {
      expect(port).toBe(3000)
    })

    test('should set by default specific port', () => {
      app = new App(5000)
      expect(app['app'].get('port')).toBe(5000)
    })
  })
})
