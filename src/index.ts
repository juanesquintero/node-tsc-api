import { App } from './app'

async function main (): Promise<void> {
  const app = new App(8000)
  await app.listen()
}

void main()
