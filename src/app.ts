import express, { Application } from 'express'

export class App {
  private readonly app: Application

  constructor (private readonly port?: number) {
    this.app = express()
    this.settings()
  }

  private settings (): void {
    this.app.set('port', this.port ?? process.env.PORT ?? 3000)
  }

  async listen (): Promise<void> {
    const port: number = this.app.get('port')
    await this.app.listen()
    console.log(`Project is running at http://0.0.0.0:${port}/`)
  }
}
