This Project was based on theese following coursers and the developer criteria...
https://perficient.udemy.com/course/programador-autodidacta-nodejs-typescript-tdd-clean-code/
https://www.youtube.com/watch?v=4clEduk6OQM
https://egghead.io/lessons/javascript-introduction-to-how-to-write-an-open-source-javascript-library

### Pending Chores

- [ ] Unit testing
- [x] Linting
- [x] Git Hook conventional commit
- [ ] Git Hook linting
- [ ] Git Hook tests
- [ ] JS Doc strings

### **Typescript**
> Remember that you only need Typescript in dev mode and all tsc related dependencies must be on devDependencies. In prod the actual project is on javascript in the /dist folder.

> Initialize a Typescript project with npx typescript --init
NodeJS engine V8 (Chrome V8) supports all the ES6 features, instead of browsers that supports mostly ES5.

<br>

### **NPX**
> Remember that npx is a CLI tool for executing npm packages, the main goal is to manage and install dependencies
https://www.geeksforgeeks.org/what-are-the-differences-between-npm-and-npx/

<br>

### **Linters**
> **eslint-config-standard-with-typescript** this package is for use ESLint with Javascript standard style in Typescript specifylly.
this package defines the config linting code format to javascript standard style. In *.eslintrc* you can see details.

> **JavaScript standard style** <br>
https://standardjs.com/
https://standardjs.com/rules.html
https://www.npmjs.com/package/eslint-config-standard-with-typescript

<br>

### **Git Standars**
> **Commitizen**: This is for a dynamic conventional commit message creation. With *cz-conventional-changelog* package we can config our project to use a certain conventional rules for the git commit.
https://www.npmjs.com/package/commitizen
https://egghead.io/lessons/javascript-writing-conventional-commits-with-commitizen
> ### **Git Hooks**
> **Git-commit-msg-linter**: this package help us to verify the commit message before the actual commit, it works as a git hook.
https://www.npmjs.com/package/git-commit-msg-linter <br>
> **Husky**: this package is for manage and define git hooks for several purpose.
https://www.npmjs.com/package/husky
If you notice that the hook isn't working this link may help
npm uninstall husky
npm install -D husky@4
npm install -D husky

https://stackoverflow.com/questions/50048717/lint-staged-not-running-on-precommit
> **Lint-Staged**: the gaol of this package is to only lint the files that are in the git staged area.
https://www.npmjs.com/package/lint-staged


### **Testing**
> **Jest**: this framework helps us to test our code
https://www.npmjs.com/package/jest
https://www.npmjs.com/package/ts-jest
https://www.npmjs.com/package/@types/jest
npm install --save-dev jest ts-jest @types/jest
npx jest --init
