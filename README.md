<h1 align="center">Welcome to Node Typescript API 👋</h1>
<p>
  <img alt="Version" src="https://img.shields.io/badge/version-1.0.0-blue.svg?cacheSeconds=2592000" />
  <a href="#" target="_blank">
    <img alt="License: ISC" src="https://img.shields.io/badge/License-ISC-yellow.svg" />
  </a>
</p>

> This project is a begginer level development of a NodeJS API with Typescript.

## Install

```sh
npm install
```

## Run server

```sh
npm run dev
```

## Run tests

```sh
npm run test
```

## Git commit

```sh
npm run commit
```

## Author

👤 **juanesquintero**

* Website: https://www.linkedin.com/in/juanes-quintero/
* Github: [@juanesquintero](https://github.com/juanesquintero)
* LinkedIn: [@https:\/\/www.linkedin.com\/in\/juanes-quintero\/](https://linkedin.com/in/https:\/\/www.linkedin.com\/in\/juanes-quintero\/)

## Show your support

Give a ⭐️ if this project helped you!

***
_This README was generated with ❤️ by [readme-md-generator](https://github.com/kefranabg/readme-md-generator)_
